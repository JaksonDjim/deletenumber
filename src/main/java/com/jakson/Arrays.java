package com.jakson;

/**
 * Created by Zheka on 13.02.2016.
 */
public class Arrays {

    public int[] arrayDeleteNum(int[] arr, int number) {
        int count = 0;
        for (int num : arr) {
            if (num == number)
                count++;
        }

        int[] newArr = new int[arr.length - count];
        for (int i = 0, k = 0; i < arr.length; i++) {
            if (arr[i] != number) {
                newArr[k++] = arr[i];
            }
        }
        return newArr;
    }
}