package com.jakson;

import java.util.List;

public class Collections {

    public <T extends Number> List<T> getNewList(List<T> list, T number) {
        for (int i = 0; i < list.size(); i++)
            if (list.get(i).equals(number)) {
                list.remove(i);
                i--;
            }
        return list;
    }
}